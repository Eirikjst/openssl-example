#include "crypto.hpp"
#include <iostream>

using namespace std;

int main() {

  cout << "MD5" << endl;
  cout << Crypto::hex(Crypto::md5("Test")) << endl
       << endl;

  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl
       << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl
       << endl;

  cout << "SHA-256" << endl;
  cout << Crypto::hex(Crypto::sha256("Test")) << endl
       << endl;

  cout << "SHA-512" << endl;
  cout << Crypto::hex(Crypto::sha512("Test")) << endl
       << endl;

  cout << "The derived key from the PBKDF2 algorithm" << endl;
  cout << Crypto::hex(Crypto::pbkdf2("Password", "Salt")) << endl;

  //Password = QwE
  string hash = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  string password = "aaa";
  string key = "";
  for (int i = 122; i > 64; i--) {
    cout << password << endl;
    password.at(0) = (char)i;
    for (int j = 122; j > 64; j--) {
      password.at(1) = (char)j;
      for (int k = 122; k > 64; k--) {
        password.at(2) = (char)k;
        key = Crypto::hex(Crypto::pbkdf2(password, "Saltet til Ola", 2048, 160));
        if (key.compare(hash) == 0) {
          cout << "Password found: " << password << endl;
          return 0;
        }
      }
    }
  }
  cout << "Password not found :(" << endl;
  return 0;
}
